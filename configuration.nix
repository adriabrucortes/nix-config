# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
	username = "adria";
	version = "23.05";
in

let
	home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/release-${version}.tar.gz";
    nix-ld = builtins.fetchTarball "https://github.com/Mic92/nix-ld/archive/main.tar.gz";
    nix-software-center = import (pkgs.fetchFromGitHub {
      owner = "vlinkz";
      repo = "nix-software-center";
      rev = "0.1.2";
      sha256 = "xiqF1mP8wFubdsAQ1BmfjzCgOD3YZf7EGWl9i69FTls=";
  }) {};
in

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      (import "${home-manager}/nixos")
      (import "${nix-ld}/modules/nix-ld.nix")
    ];

  # Bootloader.
  boot.loader = {
 	efi = {
		canTouchEfiVariables = true;
		# efiSysMountPoint = "/boot/efi";
	};

	grub = {
		enable = true;
		device = "nodev";
		efiSupport = true;
		useOSProber = true;
        configurationLimit = 5;
	};
  };

  networking.hostName = "nixolawenah"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Madrid";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "es_ES.UTF-8";
    LC_IDENTIFICATION = "es_ES.UTF-8";
    LC_MEASUREMENT = "es_ES.UTF-8";
    LC_MONETARY = "es_ES.UTF-8";
    LC_NAME = "es_ES.UTF-8";
    LC_NUMERIC = "es_ES.UTF-8";
    LC_PAPER = "es_ES.UTF-8";
    LC_TELEPHONE = "es_ES.UTF-8";
    LC_TIME = "es_ES.UTF-8";
  };

  # Configure console keymap
  console.keyMap = "es";

  # Enable flakes
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Run unpatched dynamic binaries on NixOS.
  programs.nix-ld.dev.enable = true;

  # Enable the XFCE Desktop Environment.
  services.xserver = {

    # Enable the X11 windowing system.
    enable = true;

    displayManager = {
    	lightdm.enable = true;
	#setupCommands = "xrandr --mode 1920x1080";
    };

    desktopManager.xfce.enable = false;

    windowManager = {

      awesome = {
	    enable = true;
	    luaModules = with pkgs.luaPackages; [
	      luarocks
		  luadbi-mysql
          lgi
	    ];
      };

      qtile = {
    	enable = false;
      };
    };
  };

  # Configure keymap in X11
  services.xserver = {
    layout = "es";
    xkbVariant = "";
  };

  # Enable xdg portals for flatpak
  xdg.portal = {
    enable = true;
    extraPortals = with pkgs; [
      xdg-desktop-portal-wlr
      xdg-desktop-portal-kde
      xdg-desktop-portal-gtk
    ];
  };

  # Services
  services.gnome.gnome-keyring.enable = true;
  services.flatpak.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Enable gnome keyring


  programs.fish = { 
    enable = true;
    promptInit = ''
      any-nix-shell fish --info-right | source
      '';
  };

  virtualisation = {
    podman = {
      enable = true;

      dockerCompat = true;

      defaultNetwork.settings.dns_enabled = true;

    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.${username} = {
    isNormalUser = true;
    description = "Adrià Brú";
    extraGroups = [ "networkmanager" "wheel" "video" "audio" "lp" "scanner" ];
    #initialPassword = "passwd";
    shell = pkgs.fish;
    packages = with pkgs; [
    #  firefox
    #  thunderbird
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  # Tell Xorg to use the nvidia driver
  services.xserver.videoDrivers = ["nvidia"];

  hardware.nvidia = {

    # Modesetting is needed for most wayland compositors
    modesetting.enable = true;

    # Use the open source version of the kernel module
    open = true;

    # Enable the nvidia settings menu
    nvidiaSettings = true;

    # Optionally, you may need to select the appropriate driver version for your specific GPU.
    package = config.boot.kernelPackages.nvidiaPackages.stable;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [

    # Utils
    neovim
    wget
    neofetch
    ranger
    gcc
    cargo
    fd
    fzf
    nodejs_20
    git
    cloudflared
    unzip
    zip
    gnumake
    cmake
    libsecret

    # Window Manager
    qtile
    awesome
    alacritty
    rofi
    polybarFull
    flameshot
    picom
    nitrogen
    dunst
    i3lock-fancy
    
    # Shell
    fish
    starship
    any-nix-shell

    # Necessary
    dolphin
    qt5ct
    xfce.thunar
    xfce.ristretto
    xarchiver
    lxappearance
    firefox
    pavucontrol
    pamixer

    # General
    vscode
    discord
    virtualbox
    podman
    distrobox
    spotify
    mailspring
    slack
    appimage-run
    nix-software-center
  ];

  # Apps configs
  programs.thunar.plugins = with pkgs.xfce; [
    thunar-archive-plugin
    thunar-volman
  ];
  
  # Fonts
  fonts.fonts = with pkgs; [
    nerdfonts
    iosevka
    noto-fonts 
    noto-fonts-emoji
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "${version}"; # Did you read the comment?
  
  
  # Home manager
  #home-manager.users.${username} = {
  #  home.stateVersion = "${version}";
  #  xsession = {
  #    pointerCursor = {
  #	size = 40;
  #	package = pkgs.nur.repos.abroisie.vimix-cursors;
  #	name = "Vimix-black-cursors";

   #   };
   # };
  #};
}
